// ===================
// CommentBox - main app wrapper
// ===================
class CommentBox extends React.Component{
	render() {
		return (
			<div className="commentBox">	
			<CommentList url={this.props.url} pollInterval={this.props.pollInterval} />
			<div class="row">
				<div class="col-xs-12  col-sm-8 col-sm-push-2">
					<CommentForm url={this.props.url} pollInterval={this.props.pollInterval} />
				</div>
			</div>
			
			</div>
		);
	}
}


// ===================
// Comment List - displays a list of comments
// ===================
class CommentList extends React.Component{

	// Constructor - begin with empty data and default poll interval of 1 second
	constructor() {
		super();
		let fetchProcess;
		this.state = {
			data: [],
			pollInterval: 1000
		};
	}

	// Component mount - fetch comments at interval
	componentDidMount() {
		this._fetchComments(); // get comments right away
		this.fetchProcess = setInterval(this._fetchComments.bind(this), this.props.pollInterval); // set up fetch process
	}

	// Component unmount - watch for memory leak!
	componentWillUnmount() {
		clearInterval(this.fetchProcess);
	}

	// Handle CommentList rendering
	render() {
		return (
			<div className="commentList">
			{this.state.data.map((comment) => {
				return(
					<Comment author={comment.author} key={comment.id}>
					{comment.text}
					</Comment>)
			})}
			</div>
			);
	}

	// Get comments from server
	_fetchComments() {
		$.ajax({
			url: this.props.url,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	}
}


// ===================
// Comment Form - allow user to add a new comment!
// ===================
class CommentForm  extends React.Component{
	// Constructor - begin with empty data and default poll interval of 1 second
	constructor() {
		super();
		this.state = {
			author: '', 
			text: '',
			url: '',
			disabled: false
		};
	}

	// Watch author input
	_handleAuthorChange(e) {
		this.setState({author: e.target.value});
	}

	// Watch text input
	_handleTextChange(e) {
		this.setState({text: e.target.value});
	}

	// Submit Event
	_handleSubmit(e) {
		// Prevent default and lock up form
		e.preventDefault();
		this.setState({disabled: true});

		// Get author and text
		let author = this.state.author.trim();
		let text = this.state.text.trim();
		if (!text || !author) {
			return;
		}

	    // Insert new comment over the server
	    this._insertNewComment({author: author, text: text});
	}

	// Clear the form and success/error message
	_clearFormMessage(){
		this.setState({
			alertClass: ``,
			alertMessage: ``,
			author: '',
			text: '',
			disabled: false
		});
	}

	// Insert new comment
	_insertNewComment(comment) {
		// Prep to clear and re-enable the form
		setTimeout(this._clearFormMessage.bind(this),this.props.pollInterval/2);

		// Make AJAX call
		$.ajax({
			url: this.props.url,
			dataType: 'json',
			type: 'POST',
			data: comment,
			success: function(data) {
				this.setState({
					alertClass: `alert alert-success`,
					alertMessage: `Comment Added! You should see your comment in no more than ${this.props.pollInterval/1000} seconds!`
				});
			}.bind(this),
			error: function(xhr, status, err) {
				this.setState({
					alertClass: `alert alert-danger`,
					alertMessage: `Comment Failed. So Sorry!`
				});
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	}

	render() {
		return (
			<form className="commentForm container-fluid" onSubmit={this._handleSubmit.bind(this)}>
				<div className="row">
					<div className="col-xs-12">
						<h3>Add a Comment</h3>
						<div className={this.state.alertClass}>{this.state.alertMessage}</div>
					</div>
				</div>

				<div className="row">
					<div className="col-xs-12">
						<input
							type="text"
							placeholder="Your name"
							value={this.state.author}
							onChange={this._handleAuthorChange.bind(this)}
							disabled={this.state.disabled}
							/>
					</div>
					<div className="col-xs-12">
						<textarea
							placeholder="Say something..."
							value={this.state.text}
							onChange={this._handleTextChange.bind(this)}
							disabled={this.state.disabled}
							/>
					</div>
				</div>
				
				<div className="row">
					<div className="col-xs-12">
						<input type="submit" value="Post" className="btn btn-primary" disabled={this.state.disabled}/>
					</div>
				</div>
			</form>
			);
	}
}

// ===================
// Comment - an individual comment
// ===================
class Comment  extends React.Component{
	rawMarkup() {
		let md = new Remarkable();
		let rawMarkup = md.render(this.props.children.toString());
		return { __html: rawMarkup };
	}

	render() {
		return (
			<div className="row comment-row">
				<div className="col-xs-12">
					<div className="comment">
						<h3 className="commentAuthor">{this.props.author}</h3>
						<span dangerouslySetInnerHTML={this.rawMarkup()} />
					</div>
				</div>
			</div>
			);
	}
}

ReactDOM.render(
	<CommentBox url="/api/comments" pollInterval={10000
	}/>,
	document.getElementById('content')
);